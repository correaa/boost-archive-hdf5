#ifdef COMPILATION_INSTRUCTIONS
(echo "#include\""$0"\"" > $0x.cpp) && clang++ -std=c++14 -Wall -Wextra -Wfatal-errors -I$HOME/prj $0x.cpp -o $0x.x -lstdc++fs `pkg-config --libs hdf5-serial`&& time $0x.x $@ && rm -f $0x.x $0x.cpp; exit
#endif

#include "../file.hpp"
#include "alf/boost/multi/array.hpp"

#include<iostream>

namespace multi = boost::multi;
using std::cout;

#define FILENAME    "extend.h5"
#define DATASETNAME "ExtendibleArray"
#define RANK         2

int main(){

	multi::array<int, 2> m = 
		{{1, 1, 1},
		 {1, 1, 1},
		 {1, 1, 1}}
	;

	multi::array<int, 2> mext =  { {2, 3, 4}, 
                                   {2, 3, 4}, 
                                   {2, 3, 4}, 
                                   {2, 3, 4}, 
                                   {2, 3, 4}, 
                                   {2, 3, 4}, 
                                   {2, 3, 4} };

#if 0


	hdf5::dataspace<> s{m.sizes<hdf5::size_t>()};
	auto&& f = hdf5::file::create("extend.h5");

//	hdf5::property p;
//	std::array<hdf5::size_t, 2> chunk_dims = {2, 5};
//	p.set_chunk(chunk_dims);

	hid_t prop = H5Pcreate (H5P_DATASET_CREATE);

#define RANK         2

    hsize_t      chunk_dims[2] = {2, 5};
	auto status = H5Pset_chunk (prop, RANK, chunk_dims);
//	hdf5::dataset<>::create<int>(f, "ExtendibleArray", s, p);
	auto dataset = H5Dcreate2(&f, "ExtendibleArray", hdf5::native<int>, &s, H5P_DEFAULT, prop, H5P_DEFAULT);
	H5Dclose(dataset);
	return 0;
#endif
//    hid_t        file;                          /* handles */
//    hid_t        dataspace;
//	hid_t        dataset;  
//    hid_t        filespace;
	hid_t memspace;
//    hid_t        prop;                     

    hsize_t      dims[2]  = {3, 3};           /* dataset dimensions at creation time */		
//    hsize_t      maxdims[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
    herr_t       status;                             
//    hsize_t      chunk_dims[2] = {2, 5};
//    int          data[3][3] = { {1, 1, 1},    /* data to write */
//                                {1, 1, 1},
//                                {1, 1, 1} };      

    /* Variables used in extending and writing to the extended portion of dataset */
//    hsize_t      size[2];
//    hsize_t      offset[2];
    hsize_t      dimsext[2] = {7, 3};         /* extend dimensions */
//    int          dataext[7][3] = { {2, 3, 4}, 
//                                   {2, 3, 4}, 
//                                   {2, 3, 4}, 
//                                   {2, 3, 4}, 
//                                   {2, 3, 4}, 
//                                   {2, 3, 4}, 
//                                   {2, 3, 4} };

    /* Variables used in reading data back */
    hsize_t      chunk_dimsr[2];
    hsize_t      dimsr[2];
    hsize_t      i, j;
    int          rdata[10][3];
    herr_t       status_n;                             
    int          rank, rank_chunk;

    /* Create the data space with unlimited dimensions. */
//    dataspace = H5Screate_simple (RANK, dims, maxdims); 

	using std::experimental::apply;
	{
		hdf5::dataspace<> s{apply([&](auto...e){return std::array<hdf5::size_t, 2>{hdf5::size_t(e)...};}, m.sizes()), std::array<hdf5::size_t, 2>{H5S_UNLIMITED, H5S_UNLIMITED}};

    /* Create a new file. If file exists its contents will be overwritten. */
//    file = H5Fcreate (FILENAME, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		auto&& f = hdf5::file::create(FILENAME);

    /* Modify dataset creation properties, i.e. enable chunking  */
//    prop = H5Pcreate (H5P_DATASET_CREATE);
		hdf5::property p;
	std::array<hdf5::size_t, 2> cd = {2, 5};
//	p.set_chunk();
//    status = H5Pset_chunk(&p, cd.size(), cd.data());
	p.set_chunk(cd);
    /* Create a new dataset within the file using chunk 
       creation properties.  */
//    dataset = H5Dcreate2(&f, DATASETNAME, hdf5::native<int>, &s,
//                        H5P_DEFAULT, &p, H5P_DEFAULT);

	auto&& d = hdf5::dataset<>::create<int>(f, "ExtendibleArray", s, p);
    /* Write data to dataset */
//    status = H5Dwrite(&d, hdf5::native<int>, H5S_ALL, H5S_ALL,
//                       H5P_DEFAULT, data);
	d.write(m);

    /* Extend the dataset. Dataset becomes 10 x 3  */
//    size[0] = dims[0]+ dimsext[0];
//    size[1] = dims[1];
//    status = H5Dset_extent (&d, size);

	std::array<hdf5::size_t, 2> sizes = {dims[0]+ dimsext[0], dims[1]};
	d.set_extent(sizes);
    /* Select a hyperslab in extended portion of dataset  */
	auto fs = d.get_space();
//    filespace = H5Dget_space (&d);
//    offset[0] = 3;
//    offset[1] = 0;
//    status = H5Sselect_hyperslab (&fs, H5S_SELECT_SET, offset, NULL,
//                                  dimsext, NULL);  
	fs.select({{3, 10}, {0, 3}});

    /* Define memory space */
//    memspace = H5Screate_simple (RANK, dimsext, NULL); 
	hdf5::dataspace<> ms{apply([&](auto...e){return std::array<hdf5::size_t, 2>{hdf5::size_t(e)...};}, mext.sizes())};//  mext.sizes<hdf5::size_t>() };
    /* Write the data to the extended portion of dataset  */
//    status = H5Dwrite (&d, H5T_NATIVE_INT, &ms, &fs,
//                       H5P_DEFAULT, dataext);
	d.write(fs, mext);

    /* Close resources */
//    status = H5Dclose (dataset);
//    status = H5Pclose (prop);
//    status = H5Sclose (dataspace);
//   status = H5Sclose (memspace);
//    status = H5Sclose (filespace);
//    status = H5Fclose (file);
	}
	return 0;
    /********************************************
     * Re-open the file and read the data back. *
     ********************************************/
	{
		hdf5::file f{FILENAME};
		hdf5::dataset<> d{f, "ExtendibleArray"};
//    file = H5Fopen (FILENAME, H5F_ACC_RDONLY, H5P_DEFAULT);
//    dataset = H5Dopen2 (&f, DATASETNAME, H5P_DEFAULT);

		auto fs = d.get_space();
//		filespace = H5Dget_space (&d);
		rank = H5Sget_simple_extent_ndims (&fs);
		status_n = H5Sget_simple_extent_dims (&fs, dimsr, NULL);
		auto ds = fs.get_extent();
		auto p = d.get_create_plist();
//		prop = H5Dget_create_plist(&d);

		if(H5D_CHUNKED == p.get_layout()){
			rank_chunk = H5Pget_chunk(&p, rank, chunk_dimsr);
		//	int c = p.get_chunk(std::array<hdf5::size_2>{});
		}
	memspace = H5Screate_simple (rank, dimsr, NULL);
    status = H5Dread(&d, H5T_NATIVE_INT, memspace, &fs,
                      H5P_DEFAULT, rdata);

    printf("\n");
    printf("Dataset: \n");
    for (j = 0; j < dimsr[0]; j++)
    {
       for (i = 0; i < dimsr[1]; i++)
           printf("%d ", rdata[j][i]);
       printf("\n");
    }

//    status = H5Pclose (prop);
//s    status = H5Dclose (dataset);
//    status = H5Sclose (filespace);
    status = H5Sclose (memspace);
//    status = H5Fclose (file);
	}
}

