#ifdef COMPILATION_INSTRUCTIONS
(echo "#include\""$0"\"" > $0x.cpp) && clang++ -std=c++14 -Wall -Wextra -Wfatal-errors -I$HOME/prj $0x.cpp -o $0x.x -lstdc++fs -lhdf5 && time $0x.x $@ && rm -f $0x.x $0x.cpp; exit
#endif

#include "../file.hpp"
#include "alf/boost/multi/array.hpp"

namespace multi = boost::multi;

int main(){
	{
	//	auto&& f = hdf5::file::create("group.h5");
	//	auto g = hdf5::group::create(f, "/MyGroup");
	//	auto g = f.create_group("/MyGroup");
		hdf5::file::create("group.h5").create_group("/MyGroup");
	}
	{
		hdf5::file&& f = hdf5::file::create("groups.h5");
		auto g1 = f.create_group("/MyGroup");
		auto g2 = f.create_group("/MyGroup/GroupA");
		auto g3 = f.create_group("GroupB");
	}
	{
		multi::array<double, 2> m1({3, 3}), m2({2, 10});
		for(int i = 0; i != 3; i++) for(int j = 0; j != 3 ; j++) m1[i][j]=j + 1;
		for(int i = 0; i != 2; i++) for(int j = 0; j != 10; j++) m2[i][j]=j + 1;
		hdf5::file f{"groups.h5"};
		//{
		//	hdf5::dataspace<> dsp1{m1.sizes<hdf5::size_t>()};
		//	auto&& ds1 = f.create_dataset("/MyGroup/dset1", dsp1);
		//	hdf5::dataset<> ds1{f, "/MyGroup/dset1", dsp1};
		//	ds1.write(m1);
		//}
		f.create_dataset("/MyGroup/dset1", m1.sizes<hdf5::size_t>())
			.write(m1);
	//	hdf5::group g{f, "/MyGroup/GroupA"};
	//	hdf5::dataspace<> s2{m2.sizes<hdf5::size_t>()};
	//	hdf5::dataset<> ds{g, "dset2", dsp2};
	//	ds.write(m2);
	//	hdf5::group g{f, "/MyGroup/GroupA"};
	//	hdf5::group{f, "/MyGroup/GroupA"}
	//		.create_dataset("dset2", m2.sizes<hdf5::size_t>())
	//			.write(m2);
		(f/"MyGroup/GroupA")
			.create_dataset("dset2", m2.sizes<hdf5::size_t>())
				.write(m2)
		;
	//	auto&& d = hdf5::dataset<>::create({f, "/MyGroup/GroupA"}, "dset2", m2.sizes<hdf5::size_t>());
	//	hdf5::dataset<>{{f, "/MyGroup/GroupA"}, "dset2", m2.sizes<hdf5::size_t>()}
	//		.write(m2);
	//	d.write(m2);
	}
}

