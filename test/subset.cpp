#ifdef COMPILATION_INSTRUCTIONS
(echo "#include\""$0"\"" > $0x.cpp) && clang++ -std=c++14 -Wall -Wextra -Wfatal-errors -I$HOME/prj $0x.cpp -o $0x.x -lstdc++fs -lhdf5 && time $0x.x $@ && rm -f $0x.x $0x.cpp; exit
#endif

#include "../file.hpp"
#include "alf/boost/multi/array.hpp"

#include<iostream>

namespace multi = boost::multi;
using std::cout;

int main(){
	{
		multi::array<double, 2> m({8, 10});
		for(int i = 0; i != 8; i++) for(int j = 0; j != 10; j++) m[i][j]=j<5?1:2;
		hdf5::file::create("subset.h5")
			.create_dataset("IntArray", m.sizes<hdf5::size_t>())
				.write(m);
	}
	{
		multi::array<double, 2> sm({3, 4});
		for(int i = 0; i != 3; ++i) for(int j = 0; j !=4; ++j) sm[i][j] = 5;

		hdf5::dataset<> d{"subset.h5", "IntArray"};
//		hdf5::dataspace<> mem{sm.sizes<hdf5::size_t>()};
//		auto s = d.get_space();
//		s.select_hyperslab(offsets, strides, counts, block);
//		d.write(d.get_space().select( {{1, 4}, {2, 6}} ), sm);
		d.write({{1, 4}, {2, 6}}, sm);

		multi::array<double, 2> r({8, 10});
		d.read(r);
		printf ("\nData in File after Subset is Written:\n");
		for(int i = 0; i != 8; ++i){
			for(int j = 0; j != 10; ++j) cout<< r[i][j] <<' ';
			cout<<'\n';
		}
	}
}

