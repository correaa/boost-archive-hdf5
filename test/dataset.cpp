#ifdef COMPILATION
$CXX-I$HOME/prj $0 -o $0x -lstdc++fs `pkg-config --libs --cflags hdf5-serial`&&$0x&&rm $0x;exit
#endif

#include "../file.hpp"
#include "alf/boost/multi/array.hpp"

namespace multi = boost::multi;

int main(){
	{
		multi::array<double, 2> m = {
			{1.,2., 4.},
			{3.,4., 5.}
		};
	//	auto const& d = 
			hdf5::file::create("dataset.h5").
				create_dataset("/dset", sizes(m))
		;
	}
	{
		multi::array<int, 2> m({2, 3}, 0.);
		{
			for(auto i:extension(m)) for(auto j : extension(m[i])) m[i][j]=i*6+j+1;
			auto&& d = hdf5::dataset<>{"dataset.h5", "/dset"};
			d.write(m);
		}
		multi::array<int, 2> m2({2, 3}, 1.);
		{
			assert( extensions(m2) == extensions(m) );
			auto const& d = hdf5::dataset<>{"dataset.h5", "/dset"};
			d.read(m2);
		}
		assert( m2 == m );
	}
	{ // https://support.hdfgroup.org/ftp/HDF5/current/src/unpacked/examples/h5_crtatt.c
		multi::array<int, 1> m = {100, 200};
		hdf5::attribute&& A = hdf5::attribute::create({"dataset.h5", "/dset"}, "Units", sizes(m));
		A.write(m);
	}
	{ // https://support.hdfgroup.org/ftp/HDF5/current/src/unpacked/examples/h5_crtgrp.c
		
	}
}

