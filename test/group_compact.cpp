#ifdef COMPILATION
$CXX -I$HOME/prj $0 -o $0x -lstdc++fs `pkg-config --libs --cflags hdf5-serial`&&$0x&&rm $0x;exit
#endif

/************************************************************

  This example shows how to create "compact-or-indexed"
  format groups, new to 1.8.  This example also illustrates
  the space savings of compact groups by creating 2 files
  which are identical except for the group format, and
  displaying the file size of each.  Both files have one
  empty group in the root group.

  This file is intended for use with HDF5 Library version 1.8

 ************************************************************/

#include "../file.hpp"

//#define FILE1       "h5ex_g_compact1.h5"
//#define FILE2       "h5ex_g_compact2.h5"
//#define GROUP       "G1"

int main(){
	{
		auto&& file = hdf5::file::create("group_compact_1.h5");
		std::cout << file.size() <<" bytes" << std::endl;

		auto&& group = file.create_group("G1");
		std::cout << file.size() <<" bytes" << std::endl;
		assert( group.info().storage() == hdf5::group::storage_type::compact );
		std::cout << file.size() <<" bytes" << std::endl;
	}
	{
		auto const&& file = hdf5::file("group_compact_1.h5");
		std::cout << file.size() <<" bytes" << std::endl;
	}
}
