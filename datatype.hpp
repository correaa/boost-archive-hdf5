#ifdef COMPILATION
$CXX $0 -o $0x -lstdc++fs `pkg-config --libs --cflags hdf5-serial`&&$0x&&rm $0x;exit
#endif
// © Alfredo Correa 2019-2020

#ifndef BOOST_ARCHIVE_HDF5_DATATYPE_HPP
#define BOOST_ARCHIVE_HDF5_DATATYPE_HPP

#include "hdf5/serial/hdf5.h"

#if __cplusplus>=201703L
#include<tuple>
#else
#include<experimental/tuple> // std::experimental::apply
#endif

namespace hdf5{

//template<class T> hid_t native = -1;

//template<> hid_t native<int> = H5T_NATIVE_INT;
//template<> hid_t native<double> = H5T_NATIVE_DOUBLE;
//template<> hid_t native<int32_t> = H5T_STD_I32BE;

namespace datatype{

class compound_;

struct basic_{
	hid_t impl_;
	compound_ operator[](std::size_t n) const;
};

struct native_ : basic_{
	native_(hid_t h) : basic_{h}{}
	
	template<class T> static native_ _();
};

template<> native_ native_::_<int        >(){return H5T_NATIVE_INT;    }
template<> native_ native_::_<float      >(){return H5T_NATIVE_FLOAT;  }
template<> native_ native_::_<char       >(){return H5T_NATIVE_CHAR;   }
template<> native_ native_::_<double     >(){return H5T_NATIVE_DOUBLE; }
template<> native_ native_::_<long double>(){return H5T_NATIVE_LDOUBLE;}

native_ const int_        {native_::_<int>()        };
native_ const float_      {native_::_<float>()      };
native_ const char_       {native_::_<char>()       };
native_ const double_     {native_::_<double>()     };
native_ const long_double_{native_::_<long double>()};

class compound_ : basic_{
	hid_t impl_;
	compound_(basic_ const& other) : impl_{H5Tcopy(other.impl_)}{}
public:
	hid_t operator&() const{return impl_;}
	compound_(compound_ const& other) : compound_{static_cast<basic_ const&>(other)}{}
//	compound_ operator[](std::size_t n) const{compound_ ret = *this; H5Tset_size(ret.impl_, n); return ret;}
	friend struct basic_;
public:
	~compound_(){H5Tclose(impl_);}
};

compound_ basic_::operator[](std::size_t n) const{
	compound_ ret{*this}; H5Tset_size(ret.impl_, n); return ret;
}

}}

#if not __INCLUDE_LEVEL__ // def _TEST_BOOST_ARCHIVE_HDF5_FILE

int main(){
	auto double5 = hdf5::datatype::char_[5];
}

#endif
#endif

