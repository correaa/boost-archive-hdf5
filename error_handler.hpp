#ifdef COMPILATION
$CXX $0 -o $0x `pkg-config --libs --cflags hdf5-serial`&&$0x&&rm $0x;exit
#endif
// © Alfredo Correa 2019-2020

#ifndef BOOST_ARCHIVE_HDF5_ERROR_HANDLER_HPP
#define BOOST_ARCHIVE_HDF5_ERROR_HANDLER_HPP

#include "hdf5/serial/hdf5.h"

#include "./config/MAYBE_UNUSED.hpp"

#include<cassert>

namespace hdf5{

class error_handler_t{
//	void* data_;
	template<class T> static herr_t f(void* data){(*(T*)(data))(); return 0;}
//	error_handler_t(std::nullptr_t n) : data_{n}{}
public:
//	template<class T> error_handler_t(T const& t){}
/*	template<class T>
	error_handler_t& operator=(T& t){
		herr_t s = H5Eset_auto1(&(f<T>) , (void*)&t);
		assert( s == 0 );
		return *this;
	}*/
	static error_handler_t off;
};

bool const error_handler_turned_off = []{
	H5Eset_auto(H5E_DEFAULT, NULL, NULL); return true;
}();

}

#if not __INCLUDE_LEVEL__

#include<iostream>

void my_error_handler(){
	std::cerr<<"An HDF5 error was detected. Bye.\n";
	exit (1);
}

int main(){

//	hdf5::error_handler = my_error_handler;

}

#endif
#endif

