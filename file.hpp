#ifdef COMPILATION
$CXX -I$HOME/prj $0 -o $0x -lstdc++fs `pkg-config --libs --cflags hdf5-serial`&&$0x&&rm $0x;exit
#endif
// © Alfredo Correa 2019-2020

#ifndef BOOST_ARCHIVE_HDF5_FILE_HPP
#define BOOST_ARCHIVE_HDF5_FILE_HPP

#include "hdf5/serial/hdf5.h"

#include "./datatype.hpp"

#include "./config/MAYBE_UNUSED.hpp"

#include<experimental/filesystem>
#include<cassert>

#if __cplusplus>=201703L
#include<tuple>
#else
#include<experimental/tuple> // std::experimental::apply
#endif

#include "alf/boost/multi/array.hpp"

namespace hdf5{

using std::experimental::filesystem::path;
using boost::multi::irange;

using size_t = hsize_t;

template<std::ptrdiff_t D = -1> class dataspace;

template<std::ptrdiff_t D = -1> class dataset;
class group;

class property{
protected:
	hid_t impl_;
	template<std::ptrdiff_t> friend class dataset;
	property(hid_t impl) : impl_{impl}{}
public:
	auto operator&(){return impl_;}
	property() : impl_{H5Pcreate(H5P_DATASET_CREATE)}{}
	~property(){if(impl_) H5Pclose(impl_);}
	template<class Dims>
	property& set_chunk(Dims const& dims){
		MAYBE_UNUSED auto s = H5Pset_chunk(impl_, dims.size(), dims.data()); assert(!s);
		return *this;
	}
};

struct file_access : property{
	file_access() : property{H5Pcreate(H5P_FILE_ACCESS)}{}
	file_access& set_version(){
		H5Pset_libver_bounds(impl_, H5F_LIBVER_LATEST, H5F_LIBVER_LATEST);
		return *this;
	}
};

class file{
	hid_t impl_;
	template<std::ptrdiff_t> friend class dataset;
	friend class group;
	file() : impl_{0}{}
	file(hid_t impl) : impl_{impl}{assert(impl_>=0);}
	file(file&& other) : impl_{std::exchange(other.impl_, 0)}{}
public:
	auto operator&(){return impl_;}
	static file create(path p){
		file ret;
		file_access fapl; fapl.set_version();
//		auto fapl = H5Pcreate(H5P_FILE_ACCESS);
//		H5Pset_libver_bounds(fapl., H5F_LIBVER_LATEST, H5F_LIBVER_LATEST);
		ret.impl_ = H5Fcreate(p.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, &fapl);
//		H5Pclose(fapl);
		return ret;
	}
	static file create(path p, file_access fapl){
		file ret; ret.impl_ = H5Fcreate(p.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, &fapl); return ret;
	}
	static file open_(path p){file ret; ret.impl_ = H5Fopen(p.c_str(), H5F_ACC_RDWR, H5P_DEFAULT); return ret;}
	file(file const&) = delete;
	file(path const& p) : file{open_(p)}{}
	file& operator=(file const&) = delete;
	~file(){if(impl_) H5Fclose(impl_);}
	dataset<> open(path p) const;
	group create_group(path const& p);
	dataset<> create_dataset(path const& p, dataspace<> const& s) const;
	friend group operator/(file const& f, path const& p);//{return {f, "/"+p.string()};}
	friend group operator/(file const& f, char const* p);//{return f / path(p);}
	std::string name() const{
		std::string ret(H5Fget_name(impl_, NULL, 0), ' ');
		H5Fget_name(impl_, &ret[0], ret.size()+1);
		return ret;	
	}
	size_t size() const{
		size_t r;
		MAYBE_UNUSED auto s = H5Fget_filesize(impl_, &r); assert(s==0);
		return r;
	}
};

class attribute;

#if __cpp_lib_apply>=201603
using std::apply;
#else //if __cpp_lib_experimental_apply>=201402 
using std::experimental::apply;
#endif

template<>
class dataspace<-1>{
	hid_t impl_;
	template<std::ptrdiff_t> friend class dataset;
	friend class attribute;
	dataspace(hid_t impl) : impl_{impl}{}
public:
	auto operator&(){return impl_;}
//	template<class Layout>
//	dataspace(Layout const& l){
//		hsize_t dims[typename Layout::rank{}];
//		for(int i = 0; i != typename Layout::rank{}; ++i) dims[i] = l.size(i);
//		impl_ = H5Screate_simple(typename Layout::rank{}, dims, NULL);
//	}
	template<class Dims> 
	dataspace(Dims const& ds) : impl_{H5Screate_simple(ds.size(), ds.data(), NULL)}{}
	template<class... T>
	dataspace(std::tuple<T...> const& t) : dataspace{apply([](auto... x){return std::array<hdf5::size_t, sizeof...(T)>{static_cast<hdf5::size_t>(x)...};}, t)}{}
	template<class Dims> 
	dataspace(Dims const& ds, Dims const& maxds) : impl_{H5Screate_simple(ds.size(), ds.data(), maxds.data())}{}
//	template<class Vector, typename = decltype(Vector{}.size())>
//	dataspace(Vector const& v) : impl_{H5Screate_simple(v.size(), v.data(), NULL)}{}
	dataspace(dataspace const&) = delete;
	dataspace(dataspace&& other) : impl_{std::exchange(other.impl_, 0)}{}
	dataspace& operator=(dataspace const&) = delete;
	~dataspace(){H5Sclose(impl_);}
	std::vector<hdf5::size_t> get_extensions() const{
		std::vector<hdf5::size_t> ret(H5Sget_simple_extent_ndims(impl_));
		herr_t status = H5Sget_simple_extent_dims(impl_, ret.data(), NULL); (void)status;
		return ret;
	}
	std::vector<hdf5::size_t> get_extent() const{return get_extensions();}
	template<class Offsets, class Strides, class Counts, class Block>
	dataspace& select_hyperslab(Offsets const& o, Strides const& s, Counts const& c, Block const& b){
		herr_t status = H5Sselect_hyperslab(
			impl_, H5S_SELECT_SET, o.data(), s.data(), c.data(), b.data()
		);
		(void)status;
		return *this;
	}
	template<class Hyperslab>
	dataspace& select(Hyperslab const& hs){
		std::vector<hdf5::size_t> offsets, strides, counts, block;
		for(auto& e : hs){
			offsets.push_back(e.first());
			strides.push_back(1);
			counts.push_back(e.last() - e.first());
			block.push_back(1);
		}
		return select_hyperslab(offsets, strides, counts, block);
	}
	dataspace& select(std::vector<irange> const& hyperslab){
		return select<std::vector<irange>>(hyperslab);
	}
};

template<std::ptrdiff_t D>
class dataspace : dataspace<>{
	std::array<hdf5::size_t, D> get_extensions() const{
		auto v = dataspace<>::get_extensions(); assert(v.size()==D);
		return {v.begin(), v.end()};
	}
};

template<class T> hid_t native = -1;

template<> hid_t native<int> = H5T_NATIVE_INT;
template<> hid_t native<double> = H5T_NATIVE_DOUBLE;
//template<> hid_t native<int32_t> = H5T_STD_I32BE;

class plist;
class property;

template<std::ptrdiff_t D>
class dataset{
	hid_t impl_;
	friend class file;
	friend class attribute;
	friend class group;
	dataset(hid_t impl) : impl_{impl}{
		if(impl < 0) throw std::runtime_error{"cannot link to dataset"};
	}
	dataset(dataset&& other) : impl_{std::exchange(other.impl_, 0)}{}
public:
	auto operator&(){return impl_;}
	static dataset create(file const& f, path const& p, dataspace<> const& s) try{
		return dataset{H5Dcreate2(f.impl_, p.c_str(), H5T_STD_I32BE, s.impl_, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)};
	}catch(...){throw std::runtime_error{"cannot create dataset with name "+p.string()+" in file"};}
	template<class T>
	static dataset create(file const& f, path const& name, dataspace<> const& s, property const& p);

	static dataset create(group const& g, path const& name, dataspace<> const& dsp);
	dataset(group const& g, path p, dataspace<> const& dsp);
	dataset(file const& f, path p) : dataset(f.open(p)){}
	dataset(path const& f, path const& data) : dataset{file(f), data}{}
	~dataset(){
		if(impl_){
			herr_t status = H5Dclose(impl_); 
			(void)status;
		}
	}
	template<class Extent>
	dataset& set_extent(Extent const& ext){
		herr_t status = H5Dset_extent(impl_, ext.data());
		(void)status;
		return *this;
	}
	template<class T, size_t N>
	void write(T(&t)[N]){
		herr_t status = H5Dwrite(impl_, native<typename std::remove_extent<T>::type>, H5S_ALL, H5S_ALL, H5P_DEFAULT, t);
		(void)status;
	}
	dataspace<> get_space() const{return {H5Dget_space(impl_)};}
	template<class T, size_t N>
	void read(T(&t)[N]) const{
		herr_t status = H5Dread(impl_, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, t);
		(void)status;
	}
	template<class M>
	void write(M const& m){
		herr_t status = H5Dwrite(impl_, native<typename M::element>, H5S_ALL    , H5S_ALL     , H5P_DEFAULT, m.data());
		(void)status;
	}
	template<class M>
	void write(dataspace<> const& ms, dataspace<> const& s, M const& m){
		herr_t status = H5Dwrite(impl_, native<typename M::element>, ms.impl_, s.impl_, H5P_DEFAULT, m.data());
		(void)status;
	}
	template<class M>
	void write(dataspace<> const& s, M const& m){
		using std::experimental::apply;
		return write(apply([&](auto...e){return std::array<hdf5::size_t, 2>{hdf5::size_t(e)...};}, m.sizes()), s, m);
	}
	template<class M>
	void write(std::vector<irange> const& hyperslab, M const& m){
		return write(get_space().select(hyperslab), m);
	}
	template<class M>
	void read(M&& m) const{
		herr_t status = H5Dread (impl_, native<typename std::decay_t<M>::element>, H5S_ALL, H5S_ALL, H5P_DEFAULT, m.data());
		(void)status;
	}
	plist create_plist() const;
	plist get_create_plist() const;
//	attribute create_attribute(std::string name, datatype::
};

dataset<> file::open(path p) const{return {H5Dopen2(impl_, p.c_str(), H5P_DEFAULT)};}

}

namespace hdf5{

class attribute{
	hid_t impl_;
	attribute(hid_t impl) : impl_{impl}{}
	attribute(attribute&& other) : impl_{std::exchange(other.impl_, 0)}{}
	attribute(attribute const&) = delete;
public:
	static attribute create(dataset<> const& d, std::string const& name, dataspace<> const& s){
		return {H5Acreate2(d.impl_, name.c_str(), H5T_STD_I32BE, s.impl_, H5P_DEFAULT, H5P_DEFAULT)};
	}
	~attribute(){H5Aclose(impl_);}
	template<class MA>
	void write(MA const& ma){
		herr_t status = H5Awrite(impl_, native<typename MA::element>, ma.data());
		(void)status;
	}
	void write(std::string const& s){
		herr_t status = H5Awrite(impl_, &datatype::char_[s.size()], s.data());
		assert( status >= 0);
	}
};

class group{
	hid_t impl_;
	template<std::ptrdiff_t> friend class dataset;
	group(hid_t impl) : impl_{impl}{if(impl < 0) throw std::runtime_error("cannot link group");}
public:
	static group create(file const& f, path const& p) try{
		return group{H5Gcreate(f.impl_, p.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)};
	}catch(...){throw std::runtime_error{"cannot create group "+ p.string() +" in file"};}
	static group create(group const& g, path const& p) try{
		return group{H5Gcreate(g.impl_, p.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)};
	}catch(...){throw std::runtime_error{"cannot create group "+ p.string()+" in group"};}
//	group(file const& f, path const& p) 
//	: impl_{H5Gcreate(f.impl_, p.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)}
//	{}
	group(file const& f, path const& p) : impl_{H5Gopen2(f.impl_, p.c_str(), H5P_DEFAULT)}{}
//	group(group const& g, path const& p)
//	: impl_{}
//	{}
	template<std::ptrdiff_t D = -1>
	auto create_dataset(path const& name, dataspace<> const& s){
		return dataset<D>::create(*this, name, s);
	}
	enum struct storage_type{
		compact = H5G_STORAGE_TYPE_COMPACT,
		dense =	H5G_STORAGE_TYPE_DENSE,
		table = H5G_STORAGE_TYPE_SYMBOL_TABLE,
		unknown = H5G_STORAGE_TYPE_UNKNOWN
	};
	~group(){H5Gclose(impl_);}
	struct info_t : H5G_info_t{
		enum storage_type storage(){return static_cast<enum storage_type>(H5G_info_t::storage_type);}
	};
	info_t info() const{info_t ret; H5Gget_info(impl_, &ret); return ret;}
};

group operator/(file const& f, path const& p){return {f, "/"+p.string()};}
group operator/(file const& f, char const* p){return f/path(p);}

template<> dataset<>::dataset(group const& g, path p, dataspace<> const& dsp) 
	: impl_{H5Dcreate2(g.impl_, p.c_str(), H5T_STD_I32BE, dsp.impl_, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)}
{}

template<> dataset<> dataset<>::create(group const& g, path const& p, dataspace<> const& s){
	return {H5Dcreate2(g.impl_, p.c_str(), H5T_STD_I32BE, s.impl_, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)};
}

group file::create_group(path const& p){return group::create(*this, p);}

dataset<> file::create_dataset(path const& p, dataspace<> const& s) const{
	return dataset<>::create(*this, p, s);
}

class plist{
	hid_t impl_;
	template<std::ptrdiff_t> friend class dataset;
	plist(hid_t impl) : impl_(impl){}
public:
	hid_t operator&(){return impl_;}
	plist(plist&& other) : impl_{std::exchange(other.impl_, 0)}{}
	plist(plist const&)=delete;
	~plist(){H5Pclose(impl_);}
	auto get_layout() const{return H5Pget_layout(impl_);}
};

template<> plist dataset<>::create_plist() const{return {H5Dget_create_plist(impl_)};}
template<> plist dataset<>::get_create_plist() const{return {H5Dget_create_plist(impl_)};}

template<> template<class T>
dataset<> dataset<>::create(file const& f, path const& name, dataspace<> const& s, property const& p){
	return H5Dcreate2(f.impl_, name.c_str(), native<T>, s.impl_, H5P_DEFAULT, p.impl_, H5P_DEFAULT);
}

}

#if not __INCLUDE_LEVEL__ // def _TEST_BOOST_ARCHIVE_HDF5_FILE

#include "alf/boost/multi/array.hpp"

namespace multi = boost::multi;

int main(){
	auto&& f = hdf5::file::create("dataset.h5");
	multi::array<double, 2> m = {{1.,2., 4.},{3.,4., 5.}};
	hdf5::dataspace<> dsp{sizes(m)};
	/*auto&& ds =*/
	//	hdf5::dataset<>::create(f, "/dset", dsp) // ok
		f.create_dataset("/dset", dsp) // ok
	//	f/hdf5::dataset<>{"/dset", dsp}
	;
}
#endif
#endif

